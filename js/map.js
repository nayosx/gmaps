/**
 * Para tener pariedad de datos y no tener conflictos
 * @param {float} lat
 * @param {float} lng
 * @returns {Object LatLng}
 */
var LatLng = function (lat, lng) {
    if (isNaN(lat)) {
        this.lat = 0.0;
    }
    else {
        if (lat === null || typeof lat === 'undefined' || typeof lat === 'boolean') {
            this.lat = 0.0;
        } else {
            this.lat = lat;
        }
    }

    if (isNaN(lng)) {
        this.lng = 0.0;
    }
    else {
        if (lng === null || typeof lng === 'undefined' || typeof lng === 'boolean') {
            this.lng = 0.0;
        } else {
            this.lng = lng;
        }
    }
};

var that = this;
var map;
var map_canvas;
var content_map;
var searchLocation;
var name_searchLocation;
var ERRORGEO = false;
var CREATEMAP = false;
var objLatLng = new LatLng();
var ArrayLatLng = new Array();

function initMap(callbackGeo) {
    callbackGeo();
}

function initGeoLocation(){
    pleasewait();
    if (navigator.geolocation){
        var timeoutVal = 10 * 1000 * 1000;
        navigator.geolocation.getCurrentPosition(
                showPosition,
                onError,
                {enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0}
        );
    } else {
        alert("Geolocation is not supported by this browser");
    }
}

function showPosition(position){
    objLatLng.lat = position.coords.latitude;
    objLatLng.lng = position.coords.longitude;
    console.log(objLatLng);
    CREATEMAP = true;
    addLatLng(objLatLng);
    GMap(objLatLng);
}

function onError(error) {
    unpleasewait();
    ERRORGEO = true;
    var errors = {
        1: 'Permission denied',
        2: 'Position unavailable',
        3: 'Request timeout',
        4: 'Unknown error'
    };
    alert(errors[error.code]);
    console.log("Error: " + errors[error.code]);
    addLatLng(objLatLng);
    GMap(objLatLng);
}

function addLatLng(LatLng) {
    ArrayLatLng.push(LatLng);
}

function setMapCanvas(id_canvas){
    map_canvas = document.getElementById(id_canvas);
}

function setInputSearch(id_input_search) {
    name_searchLocation = id_input_search;
    searchLocation = document.getElementById(id_input_search);
}

function getMapCanvas(){
    return map_canvas;
}

function getLatLng(){
    return objLatLng;
}

function setLatLng(location) {
    objLatLng.lat = location.lat();
    objLatLng.lng = location.lng();
}

function GMap(LatLng){
    var marker;
    var mapOptions;
    var searchBox = new google.maps.places.SearchBox((searchLocation));
    var pos = new google.maps.LatLng(LatLng.lat, LatLng.lng);

    if(CREATEMAP){
        mapOptions = {
            zoom: 16,
            center: LatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
    }
    if(ERRORGEO){
        mapOptions = {
            zoom: 2,
            center: LatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
    }
/* Metodos principales para cargar los mapas */
    map = new google.maps.Map(map_canvas, mapOptions);
    marker = placeMarker(pos, map, null);
    google.maps.event.addListenerOnce(map, 'idle', function () {
    });
    google.maps.event.addListener(map, 'tilesloaded', function(evt) {
        marker = placeMarker(getLatLng(), map, marker);
        unpleasewait();
    });
/* Termina metodos*/

    map.addListener('center_changed', function () {
    });

    google.maps.event.addListener(searchBox, 'places_changed', function(){
        var places = searchBox.getPlaces();
        if (places.length == 0) {
            return;
        }
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0, place; place = places[i]; i++) {
            setLatLng(place.geometry.location);
            marker = placeMarker(place.geometry.location, map, marker);
            bounds.extend(place.geometry.location);
            console.log(getLatLng());
        }
        map.fitBounds(bounds);
        map.setZoom(17);
    });

    google.maps.event.addListener(map, 'click', function( event ){
        setLatLng(event.latLng);
        marker = placeMarker(event.latLng, map, marker);
        console.log(getLatLng());
        window.setTimeout(function() {
            map.panTo(getLatLng());
        },500);
//        addLatLng(new that.LatLng(event.latLng.lat(), event.latLng.lng()));
//        console.log(ArrayLatLng);
    });

    google.maps.event.addListener(marker, 'drag', function (event) {
        setLatLng(event.latLng);
        console.log(getLatLng());
    });
    google.maps.event.addListener(marker, 'dragend', function (event) {
        setLatLng(event.latLng);
        console.log(getLatLng());
        window.setTimeout(function() {
            map.panTo(getLatLng());
        },500);
    });
}

function placeMarker(location, map, marker) {
    if(marker){
        marker.setPosition(location);

    } else{
        marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: true,
        });

        marker.addListener('click', toggleBounce);
    }
    function toggleBounce(){
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }
    return marker;
}

function resizeMap(idContentMap, heightmap){
    content_map = document.getElementById(idContentMap);
    var w = content_map.offsetWidth;
    map_canvas.style.width = (w - 20 ) + "px";
    if(typeof heightmap !== 'undefined'){
      map_canvas.style.height = heightmap + "px";
    }
    else {
      map_canvas.style.height = (3 * w / 4) + "px";
    }
    return map_canvas;
}

function getGoogleMap() {
    google.maps.event.addDomListener(window, 'load', GMap);
}
