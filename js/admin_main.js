var block_screen;
function pleasewait(language){
  block_screen = $.blockUI({css: {
          border: 'none',
          padding: '20px',
          backgroundColor: '#000',
          '-webkit-border-radius': '10px',
          '-moz-border-radius': '10px',
          opacity: .6,
          color: '#fff'
      },
      message: '<i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i> Espere por favor....',
  });
}
function unpleasewait(){
    $.unblockUI(block_screen);
}
