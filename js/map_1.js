/**
 * Para tener pariedad de datos y no tener conflictos
 * @param {float} lat
 * @param {float} lng
 * @returns {Object LatLng}
 */
var LatLng = function (lat, lng) {
    if (isNaN(lat)) {
        this.lat = 0.0;
    }
    else {
        if (lat === null || typeof lat === 'undefined' || typeof lat === 'boolean') {
            this.lat = 0.0;
        } else {
            this.lat = lat;
        }
    }

    if (isNaN(lng)) {
        this.lng = 0.0;
    }
    else {
        if (lng === null || typeof lng === 'undefined' || typeof lng === 'boolean') {
            this.lng = 0.0;
        } else {
            this.lng = lng;
        }
    }
};

/**
 * 
 * @param {Object} LatLng
 * @returns {ManagerMap}
 */
var ManagerMap = function (LatLng) {
    var that = this;
    var map_canvas;
    var LatLng = LatLng;
    var ERRORGEO = false;
    var CREATEMAP = false;
    
    this.setMapCanvas = function(id_canvas){
        map_canvas = document.getElementById(id_canvas);
    };

    this.getMapCanvas = function(){
        return map_canvas;
    };
    
    this.getLatLng = function(){
        return LatLng;
    };
    
    this.resizeMap = function(idContentMap){
        var content_map = document.getElementById(idContentMap);
        var w = content_map.offsetWidth;
        map_canvas.style.width = (w - 5 ) + "px";
        map_canvas.style.height = (3 * w / 4) + "px";
        return map_canvas;
    };

    this.initGeoLocation = function(){
        pleasewait();
        if (navigator.geolocation){
            var timeoutVal = 10 * 1000 * 1000;
            navigator.geolocation.getCurrentPosition(
                    that.showPosition,
                    that.onError,
                    {enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0}
            );
        } else {
            alert("Geolocation is not supported by this browser");
        }
    };

    this.showPosition = function (position){
        LatLng.lat = position.coords.latitude;
        LatLng.lng = position.coords.longitude;
        CREATEMAP = true;
    };

    this.onError = function (error) {
        ERRORGEO = true;
        unpleasewait();
        var errors = {
            1: 'Permission denied',
            2: 'Position unavailable',
            3: 'Request timeout',
            4: 'Unknown error'
        };
        alert(errors[error.code]);
        console.log("Error: " + errors[error.code]);
    };
};

var ArrayLatLng = new Array();


this.addLatLng = function (LatLng) {
    ArrayLatLng.push(LatLng);
};

var Map = function (LatLng){
    var mapOptions;
    var manager = new ManagerMap(LatLng);
    manager.setMapCanvas('map');
    manager.initGeoLocation();
    mapOptions = {
        zoom: 4,
        center: manager.getLatLng(),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    
    map = new google.maps.Map(manager.getMapCanvas(), mapOptions);
    google.maps.event.addListenerOnce(map, 'idle', function () {
        console.log("fuckyou");
    });
    google.maps.event.addListener(map, 'tilesloaded', function(evt) {
        console.log("listo bro");
        map.panTo("-89.203192", "12.0000");
        unpleasewait();
    });
    
    this.getMap = function(){
        return map;
    };
    
    this.resizeCanvas = function(){
        return manager.resizeMap('mapcontainer');
    };
};

function initMap(callbackGeo) {
    callbackGeo();
}

var getGoogleMap = function () {
    google.maps.event.addDomListener(window, 'load', initGMap);
}