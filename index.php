<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="css/p.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <form id="eventoCrea" role="form" data-parsley-validate>
                        <label class="control-label">Nombre del evento</label>
                        <input type="text" id="nombre_evento" name="nombre_evento" class="form-control" required=""/>
                        
                        <label class="control-label">Fecha de evento</label>
                        <input type="text" id="fecha" class="form-control" required=""/>
                        <input type="hidden" id="fecha_evento" name="fecha_evento"/>
                        <label class="control-label"> Categoria</label>
                        <select class="form-control" id="id_categoria" name="id_categoria" required="">
                            <option>Select</option>
                        </select>
                        <label class="control-label">Tipo</label>
                        <select class="form-control" id="id_tipo_evento" name="id_tipo_evento" required="">
                            <option>Select</option>
                        </select>
                        <label class="control-label">Descripcion</label>
                        <textarea rows="3" class="form-control" id="descripcion" name="descripcion"></textarea>
                        <label class="control-label" >Dirección</label>
                        <input class="form-control" type="search" placeholder="Buscar lugar" id="pac-input" />
                    </form>
                </div>
            </div>
            <div class="row anotherrow">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8" id="mapcontainer">
                    <div id="map"></div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <button type="button" id="addLatLng" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i> Agregar nuevo marcador</button>
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-folder-close">
                                        </span>Content 1</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <i class="glyphicon glyphicon-circle-arrow-right"></i>
                                        <label class="control-label">Dirección</label>
                                        <textarea class="dir form-control" rows="3"></textarea>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 text-right">
                    <button type="button" class="btn btn-default" id="cancelEvent"> Cancelar</button>
                    <button type="button" class="btn btn-success" id="saveEvent"> Guardar</button>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="plugins/parsley/parsley.min.js"></script>
        <script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>
        <script type="text/javascript" src="plugins/moment/moment.min.js"></script>
        <script type="text/javascript" src="plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="js/admin_main.js"></script>
        <script type="text/javascript" src="js/map.js"></script>
        <!--https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing,places&sensor=true-->
        
        
        <!--<script src="https://maps.google.com/maps/api/js?sensor=true&libraries=drawing,places"></script>-->
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing,places&sensor=true"></script>
        <script type="text/javascript">
            $(function () {
                var btnCreaEvent = $("#saveEvent");
                var formCrea = $("#eventoCrea");
                var btnAddLatLng = $("#addLatLng");
                var addListLatLng = $("#accordion");
                var fecha = $("#fecha");
                var setFecha = $("#fecha_evento");
                var count_list = 2;
                var config_time = {
//                    format: 'MM/DD/YYYY LT',
                    stepping: 30,
                    sideBySide: true,
                };
                fecha.datetimepicker(config_time);
                fecha.on("dp.change", function(e){
                    var date = moment($(this).val(), "MM/DD/YYYY LT");
                    setFecha.val(date.format("YYYY-MM-DD HH:mm:ss"));
                    console.log(setFecha.val());
                });
                
                btnAddLatLng.click(function () {
                    var listContent = '<div class="panel panel-default">\
                        <div class="panel-heading">\
                            <h4 class="panel-title">\
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse' + count_list + '"><span class="glyphicon glyphicon-folder-close">\
                                    </span>Content ' + count_list + '</a>\
                            </h4>\
                        </div>\
                        <div id="collapse' + count_list + '" class="panel-collapse collapse">\
                            <ul class="list-group">\
                                <li class="list-group-item"><span class="glyphicon glyphicon-pencil text-primary"></span><a href="http://fb.com/moinakbarali">Articles</a></li>\
                                <li class="list-group-item"><span class="glyphicon glyphicon-flash text-success"></span><a href="http://fb.com/moinakbarali">News</a></li>\
                                <li class="list-group-item"><span class="glyphicon glyphicon-file text-info"></span><a href="http://fb.com/moinakbarali">Newsletters</a></li>\
                                <li class="list-group-item"> <span class="glyphicon glyphicon-comment text-success"></span><a href="http://fb.com/moinakbarali">Comments</a><span class="badge">42</span></li>\
                            </ul>\
                        </div>\
                    </div>';
                    addListLatLng.append(listContent);
                    count_list++;
                });
                setMapCanvas('map');
                setInputSearch('pac-input');
                initMap(initGeoLocation);
                
                $(window).resize(function () {
                    google.maps.event.trigger(resizeMap('mapcontainer'), 'resize');
                });
                $(window).trigger('resize');
            });
        </script>
    </body>
</html>
