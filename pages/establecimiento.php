<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../css/p.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12" id = "mapcontainer">
          <form role="form" id="craete" data-parsley-validate >
            <input type="hidden" id="id_usuario" name="id_usuario" />
            <input type="hidden" id="by_user" name="by_user" value="1" />
            <input type="hidden" id="trash" name="trash" value="1" />
            <label class="control-label">Nombre del establecimiento</label>
            <input type="text" class="form-control" id="nombre" name="nombre" required="" />
            <label class="control-label">Capacidad</label>
            <input type="number" class="form-control" id="capacidad" name="capacidad" data-parsley-type="integer" data-parsley-min="1" min="1"  required=""/>
            <label class="control-label">Estacionamiento</label>
            <input type="number" class="form-control" id="estacionamiento" name="estacionamiento" data-parsley-type="integer" data-parsley-min="0" min="0" required="" value="0"/>
            <label class="control-label">Dirección</label>
            <textarea type="text" class="form-control" id="direccion" name="direccion" rows="3" required=""></textarea>
            <input type="hidden" id="lat" name="lat" />
            <input type="hidden" id="lng" name="lng" />
            <label class="control-label">Localizar en mapa</label>
            <input class="form-control" type="search" placeholder="Buscar lugar" id="pac-input" />
            <br />
            <div id="map"></div>
            <br />
            <button id = "add" type = "submit" class="btn btn-success" > Guardar</button>
            <button id = "cancelar" type = "button" class="btn btn-default" > Cancelar</button>
          </form>
        </div>
      </div>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../plugins/parsley/parsley.min.js"></script>
    <script type="text/javascript" src="../plugins/blockui/jquery.blockUI.min.js"></script>
    <script type="text/javascript" src="../js/admin_main.js"></script>
    <script type="text/javascript" src="../js/map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing,places&sensor=true"></script>
    <script type="text/javascript">
        $(function () {
            var formCrea = $("#craete");
            var lat = $("#lat");
            var lng = $("#lng");
            formCrea.submit(function(e){
              e.preventDefault();
              var point = getLatLng();
              lat.val(point.lat);
              lng.val(point.lng);
              var data = {};
              $(this).serializeArray().map(function(x){ data[x.name] = x.value; });
              console.log(data);
            });

            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            setMapCanvas('map');
            setInputSearch('pac-input');
            initMap(initGeoLocation);

            $(window).resize(function () {
                google.maps.event.trigger(resizeMap('mapcontainer', 420), 'resize');
            });
            $(window).trigger('resize');
        });
    </script>
    </body>
    </html>
