<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="css/p.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <form id="eventoCrea" role="form">
                        <label class="control-label">Nombre del evento</label>
                        <input type="text" id="nombre_evento" name="nombre_evento" class="form-control" />
                        <label class="control-label">Fecha de evento</label>
                        <input type="text" id="fecha_evento" name="fecha_evento" class="form-control" />
                        <label class="control-label">Descripcion</label>
                        <textarea rows="3" class="form-control" id="descripcion" name="descripcion"></textarea>
                    </form>
                </div>
            </div>
            
            <div class="row">
                        <div class="col-sm-9 col-md-9 col-lg-9" id="mapcontainer">
                            <div id="map"></div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <button type="button" id="addLatLng" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i> Agregar nuevo marcador</button>
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-folder-close">
                                                </span>Content 1</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <ul class="list-group">
                                            <li class="list-group-item"><span class="glyphicon glyphicon-pencil text-primary"></span><a href="http://fb.com/moinakbarali">Articles</a></li>
                                            <li class="list-group-item"><span class="glyphicon glyphicon-flash text-success"></span><a href="http://fb.com/moinakbarali">News</a></li>
                                            <li class="list-group-item"><span class="glyphicon glyphicon-file text-info"></span><a href="http://fb.com/moinakbarali">Newsletters</a></li>
                                            <li class="list-group-item"> <span class="glyphicon glyphicon-comment text-success"></span><a href="http://fb.com/moinakbarali">Comments</a><span class="badge">42</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Información general</a></li>
                <li><a data-toggle="tab" href="#menu1">Marcadores en mapa</a></li>
            </ul>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <h3>HOME</h3>
                    
                </div>
                <div id="menu1" class="tab-pane fade">
                    <h3>Menu 1</h3>
                    
                </div>
            </div>
        </div>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>
        <script type="text/javascript" src="plugins/moment/moment.min.js"></script>
        <script type="text/javascript" src="plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="js/admin_main.js"></script>
        <script type="text/javascript" src="js/map.js"></script>
        <script type="text/javascript">
            $(function () {
                var btnAddLatLng = $("#addLatLng");
                var addListLatLng = $("#accordion");
                var count_list = 2;
                var latlng = new LatLng();
                var mManager = new ManagerMap(latlng);

                var config_time = {
                    format: 'MM/DD/YYYY LT',
                    stepping: 30,
                    sideBySide: true
                };
                $("#fecha_evento").datetimepicker(config_time);
                mManager.setCanvasMap('map');
                mManager.resizeMap('mapcontainer');
                mManager.initGeoLocation();

                btnAddLatLng.click(function () {
                    var listContent = '<div class="panel panel-default">\
                        <div class="panel-heading">\
                            <h4 class="panel-title">\
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse' + count_list + '"><span class="glyphicon glyphicon-folder-close">\
                                    </span>Content ' + count_list + '</a>\
                            </h4>\
                        </div>\
                        <div id="collapse' + count_list + '" class="panel-collapse collapse">\
                            <ul class="list-group">\
                                <li class="list-group-item"><span class="glyphicon glyphicon-pencil text-primary"></span><a href="http://fb.com/moinakbarali">Articles</a></li>\
                                <li class="list-group-item"><span class="glyphicon glyphicon-flash text-success"></span><a href="http://fb.com/moinakbarali">News</a></li>\
                                <li class="list-group-item"><span class="glyphicon glyphicon-file text-info"></span><a href="http://fb.com/moinakbarali">Newsletters</a></li>\
                                <li class="list-group-item"> <span class="glyphicon glyphicon-comment text-success"></span><a href="http://fb.com/moinakbarali">Comments</a><span class="badge">42</span></li>\
                            </ul>\
                        </div>\
                    </div>';
                    addListLatLng.append(listContent);
                    count_list++;
                });
            });
        </script>
    </body>
</html>
